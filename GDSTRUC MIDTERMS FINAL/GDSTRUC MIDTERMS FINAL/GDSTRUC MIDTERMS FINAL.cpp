// GDSTRUC MIDTERMS FINAL.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	cout << "Enter size of array: ";

	int size;

	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	while (true)
	{


		for (int i = 0; i < size; i++)
		{
			int rng = rand() % 101;
			unordered.push(rng);
			ordered.push(rng);
		}


		cout << "\nGenerated array: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";

		cout << endl;

		int playerDecision;
		cout << "Hi Sir Dale! Whatchu wanna do?" << endl;
		cout << "[1] - Remove an index" << endl;
		cout << "[2] - Search an element" << endl;
		cout << "[3] - Expand the current array?" << endl;
		cin >> playerDecision;

		if (playerDecision == 1)
		{

			int removeElement;

			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << " ";

			cout << endl;
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << " ";



			cout << "What element do you want removed?" << endl;
			cin >> removeElement;

			unordered.remove(removeElement);
			ordered.remove(removeElement);

			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << " ";

			cout << endl;

			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << " ";

			cout << endl;

		}

		else if (playerDecision == 2)
		{


			cout << "\n\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;


		}
		else if (playerDecision == 3)
		{
		
			int addSpace;

			cout << "How many?" << endl;
			cin >> addSpace;


			for (int i = 0; i < addSpace; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}

			cout << "\nGenerated array: " << endl;
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
		}
	}
	system("pause");
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
