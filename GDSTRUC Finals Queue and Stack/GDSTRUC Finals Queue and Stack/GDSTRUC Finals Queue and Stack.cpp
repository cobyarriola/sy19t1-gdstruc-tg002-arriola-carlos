#include "pch.h"
#include <iostream>
#include "Queue.h"
#include "Stack.h"
#include <time.h>
#include <string>

using namespace std;

void printElements(Queue<int> &queue, Stack<int> &stack)
{
	cout << "Queue elements: " << endl;
	for (int i = 0; i < queue.getSize(); i++)
		cout << queue[i] << endl;
	cout << "Stack elements: " << endl;
	for (int i = stack.getSize() - 1; i >= 0; i--)
		cout << stack[i] << endl;
}

void inputElement(Queue<int> &queue, Stack<int> &stack)
{
	int input;
	cout << "Enter number: ";
	cin >> input;

	queue.push(input);
	stack.push(input);
}

void printTop(Queue<int> &queue, Stack<int> &stack)
{
	cout << "Top of element sets: " << endl;
	cout << "Queue: ";
	cout << queue.top() << endl;

	cout << "Stack: ";
	cout << stack.top() << endl;
}

void popTop(Queue<int> &queue, Stack<int> &stack)
{
	cout << "You have popped the front elements." << endl;

	queue.pop();
	stack.pop();
}

void emptySet(Queue<int> &queue, Stack<int> &stack)
{
	cout << "Queue elements: " << endl;
	for (int i = queue.getSize() - 1; i >= 0; i--)
	{
		cout << queue[i] << endl;
	}
	for (int i = queue.getSize() - 1; i >= 0; i--)
	{
		queue.pop();
	}
	cout << endl;
	cout << "Stack elements: " << endl;
	for (int i = stack.getSize() - 1; i >= 0; i--)
	{
		cout << stack[i] << endl;
	}
	for (int i = stack.getSize() - 1; i >= 0; i--)
	{
		stack.pop();
	}
	cout << endl;
}

int main()
{
	int size;
	int i;
	cout << "Enter size for element sets: ";
	cin >> size;

	Queue<int> queue(size);
	Stack<int> stack(size);

	while (true)
	{
		int userChoice;
		cout << "What do you want to do?" << endl;
		cout << "1 - Push elements" << endl;
		cout << "2 - Pop elements" << endl;
		cout << "3 - Print everything then empty set" << endl;
		cin >> userChoice;

		switch (userChoice)
		{
		case 1:
			inputElement(queue, stack);
			printTop(queue, stack);
			system("pause");
			system("CLS");
			break;
		case 2:
			popTop(queue, stack);
			system("pause");
			system("cls");
			break;
		case 3:
			emptySet(queue, stack);
			system("pause");
			system("cls");
		}
	}
}